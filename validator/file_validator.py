

class Component:
    def __init__(self, name, extension, depth):
        self.name = name
        self.extension = extension
        self.depth = depth

    def compare(self, other):
        return self.name == other.name and self.extension == other.extension and self.depth == other.depth


class Archetype:
    def __init__(self, name, description, componentList):
        self.name = name
        self.description = description
        self.componentList = componentList